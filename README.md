# ID721 Individual Project 4

### Requirements
* Rust AWS Lambda function
* Step Functions workflow coordinating Lambdas
* Orchestrate data processing pipeline

### Demo Video
Demo Video is in the root directory.

### Steps
#### Developing Rust Lambda Function
1. Create Rust Project
`cargo new rust_lambda`
`cd rust_lambda`

2. Add Dependencies to `Cargo.toml`
```
lambda_runtime = "0.4.1"
tokio = { version = "1", features = ["full"] }
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
log = "0.4"
simple_logger = "1.0"
chrono = "0.4"
```

3. Implement the Lambda Handler
```
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::json;
use chrono::prelude::*;

#[derive(Deserialize)]
struct SalesInput {
    date: String,
    amount: f64,
    currency: String,
    region: String,
}

#[derive(Serialize)]
struct SalesOutput {
    timestamp: i64,
    amount_usd: f64,
    tax: f64,
}

async fn handler(event: SalesInput, _: Context) -> Result<SalesOutput, Error> {
    let date = Utc.datetime_from_str(&event.date, "%Y-%m-%d %H:%M:%S")?;
    let amount_usd = if event.currency == "EUR" {
        event.amount * 1.1 // Example conversion rate
    } else {
        event.amount
    };
    let tax = amount_usd * 0.07; // Compute 7% tax

    let response = SalesOutput {
        timestamp: date.timestamp(),
        amount_usd,
        tax,
    };

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    simple_logger::init_with_level(log::Level::Info)?;
    lambda_runtime::run(handler_fn(handler)).await?;
    Ok(())
}


```

4. Build the Cargo Project
`cargo lambda build --release`

5. Deploy the Lambda Function
`cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>`

#### Step Function
1. Open AWS Step Function in the Console
Select "Create State Machine", Choose "Author with code snippets"

2. Design the Workflow
```
{
  "Comment": "A simple AWS Step Functions state machine that processes data using a Rust Lambda function.",
  "StartAt": "ProcessData",
  "States": {
    "ProcessData": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:region:account-id:function:rustLambdaFunction",
      "End": true
    }
  }
}
```
![workflow](workflow.png)
3. Start an Execution
Provide an input JSON if your Lambda function requires it
![test](test.png)
![graph](graph.png)
