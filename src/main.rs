use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::json;
use chrono::prelude::*;

#[derive(Deserialize)]
struct SalesInput {
    date: String,
    amount: f64,
    currency: String,
    region: String,
}

#[derive(Serialize)]
struct SalesOutput {
    timestamp: i64,
    amount_usd: f64,
    tax: f64,
}

async fn handler(event: SalesInput, _: Context) -> Result<SalesOutput, Error> {
    let date = Utc.datetime_from_str(&event.date, "%Y-%m-%d %H:%M:%S")?;
    let amount_usd = if event.currency == "EUR" {
        event.amount * 1.1 // Example conversion rate
    } else {
        event.amount
    };
    let tax = amount_usd * 0.07; // Compute 7% tax

    let response = SalesOutput {
        timestamp: date.timestamp(),
        amount_usd,
        tax,
    };

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    simple_logger::init_with_level(log::Level::Info)?;
    lambda_runtime::run(handler_fn(handler)).await?;
    Ok(())
}

